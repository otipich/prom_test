package main.com.promtest;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import main.com.promtest.core.App;

public class Utils {

    public static void appendInfo(TextView tv, String symbol, String... info) {
        StringBuilder stringBuilder = new StringBuilder();
        if (info.length > 2) {
            stringBuilder.append(info[2]).append(" | ");
        }
        stringBuilder.append(info[0]).append(symbol).append(info[1]);
        tv.setText(stringBuilder.toString());
    }

    public static void setEnabledDisabledView(boolean isEnabled, TextView... textViews) {
        for (TextView tv : textViews) {
            int color = isEnabled ? android.R.color.black : R.color.colorDisabled;
            tv.setTextColor(getColor(color));
        }
    }


    public static int getColor(int id) {
        return App.getContext().getResources().getColor(id);
    }

    public static String format(String input, int res) {
        String result;
        result = String.format(App.getContext().getResources().getString(res), input);
        return result;
    }

    public static String format(int input, int res) {
        return format(String.valueOf(input), res);
    }


    public static void showKeyboard(EditText et) {
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) App.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    public static String getString(int id) {
        return App.getContext().getResources().getString(id);
    }

    public static boolean hasConnection() {
        InternetStatus status = getInternetStatus(App.getContext());
        return status == InternetStatus.HAVE_GPRS || status == InternetStatus.HAVE_WIFI;
    }

    private static InternetStatus getInternetStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) return InternetStatus.NO_INTERNET;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        if (netInfo == null) return InternetStatus.NO_INTERNET;
        for (NetworkInfo ni : netInfo) {
            if ("wifi".equalsIgnoreCase(ni.getTypeName()) && ni.isConnected())
                return InternetStatus.HAVE_WIFI;
            else if ("mobile".equalsIgnoreCase(ni.getTypeName()) && ni.isConnected())
                return InternetStatus.HAVE_GPRS;
        }
        return InternetStatus.NO_INTERNET;
    }

    public enum InternetStatus {
        NO_INTERNET,
        HAVE_GPRS,
        HAVE_WIFI,
    }


}
