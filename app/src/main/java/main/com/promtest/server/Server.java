package main.com.promtest.server;

import main.com.promtest.data.Orders;
import main.com.promtest.server.request.ICallback;
import main.com.promtest.server.request.IRequestGetAllOrders;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


public class Server {

    private static Retrofit getRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl("http://my.prom.ua/cabinet/")
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }


    public static void getAllOrders(final ICallback iCallback) {
        IRequestGetAllOrders service = getRetrofitBuilder().create(IRequestGetAllOrders.class);
        Call<Orders> call = service.getAll();
        call.enqueue(new Callback<Orders>() {
            @Override
            public void onResponse(Call<Orders> call, Response<Orders> response) {
                iCallback.onResponseResult(response.body());
            }

            @Override
            public void onFailure(Call<Orders> call, Throwable t) {
                t.fillInStackTrace();
            }
        });
    }


}
