package main.com.promtest.server.request;


import main.com.promtest.data.CommonData;

public interface ICallback {
    void onResponseResult(CommonData commonData);
}
