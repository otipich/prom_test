package main.com.promtest.server.request;

import main.com.promtest.data.Orders;
import retrofit2.Call;
import retrofit2.http.GET;


public interface IRequestGetAllOrders {
    @GET("export_orders/xml/2007624?hash_tag=72dfd843406955c15f9a427793182aad")
    Call<Orders> getAll();
}
