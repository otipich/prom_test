package main.com.promtest.core;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import main.com.promtest.R;
import main.com.promtest.fragments.FrgMain;
import main.com.promtest.fragments.FrgSearch;
import main.com.promtest.server.Server;

public class ActMain extends AppCompatActivity implements View.OnClickListener {
    private TextView tvToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        if (savedInstanceState == null)
            showFragment(FrgMain.newInstance(), false);
    }


    public void showFragment(Fragment frg, boolean addToBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.act_main_fragment_container, frg, frg.getClass().getSimpleName());
        if (addToBackStack)
            ft.addToBackStack(frg.getClass().getSimpleName());
        ft.commit();
    }

    public void setTitle(String title) {
        if (getSupportActionBar() != null) {
            tvToolbarTitle.setText(title);
        }
    }

    @Override
    public void onClick(View v) {
        onBackPressed();
    }
}
