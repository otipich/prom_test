package main.com.promtest.core;


import main.com.promtest.data.Item;
import main.com.promtest.data.Order;
import main.com.promtest.data.Orders;

public class Model {
    private static Model instance = new Model();
    private Orders orders;


    public static Model getInstance() {
        return instance;
    }

    private Model() {
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
        for (Order order : getOrders().getOrdersList()) {
            for (Item item : order.getItems().getItemsList()) {
                item.setOrderId(order.getId());
            }
        }
    }

    public Order getOrderById(int id) {
        for (Order order : getOrders().getOrdersList()) {
            if (id == order.getId())
                return order;
        }
        return null;
    }

    public Order getOrderByItemId(int id) {
        for (Order order : getOrders().getOrdersList()) {
            for (Item item : order.getItems().getItemsList()) {
                if (item.getOrderId() == id)
                    return order;
            }
        }
        return null;
    }

    public Order getPreviousOrder(Order currentOrder) {
        int idx = getOrders().getOrdersList().indexOf(currentOrder);
        if (idx <= 0) return null;
        return getOrders().getOrdersList().get(idx - 1);
    }

    public Order getNextOrder(Order currentOrder) {
        int idx = getOrders().getOrdersList().indexOf(currentOrder);
        if (idx < 0 || idx + 1 == getOrders().getOrdersList().size()) return null;
        return getOrders().getOrdersList().get(idx + 1);
    }
}
