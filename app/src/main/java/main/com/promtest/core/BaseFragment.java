package main.com.promtest.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import main.com.promtest.R;
import main.com.promtest.server.Server;


public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public ActMain getActMain() {
        return (ActMain) getActivity();
    }


    @Override
    public abstract void onViewCreated(View view, @Nullable Bundle savedInstanceState);

    public void finish() {
        getFragmentManager().popBackStack();
    }

    public void setFragmentTitle(String title) {
        getActMain().setTitle(title);
    }

}
