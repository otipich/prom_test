package main.com.promtest.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import main.com.promtest.R;

import main.com.promtest.Utils;
import main.com.promtest.data.Item;

public class AdapterSearch extends BaseAdapter {
    private List<Item> items;
    private int orderId;

    public AdapterSearch(List<Item> orders) {
        this.items = orders;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_lv_search_items, parent, false);
            holder = new ViewHolder();
            holder.tvProductName = (TextView) convertView.findViewById(R.id.item_lv_search_items_product_name);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.item_lv_search_items_price);
            holder.ivLogo = (ImageView) convertView.findViewById(R.id.item_lv_search_items_logo);
            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        holder.tvProductName.setText(getItem(position).getName());
        String price = (long) getItem(position).getPrice() + " " + Utils.getString(R.string.UAH);
        holder.tvPrice.setText(price);
        String url = getItem(position).getImage();
        if (url != null) {
            Picasso.with(context).load(url).into(holder.ivLogo);
        }
        convertView.setTag(R.integer.adapter_search_list_tag, getItem(position).getOrderId());
        return convertView;
    }

    private class ViewHolder {
        public TextView tvProductName, tvPrice;
        public ImageView ivLogo;
    }
}
