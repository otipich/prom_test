package main.com.promtest.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import main.com.promtest.R;
import main.com.promtest.States;
import main.com.promtest.Utils;
import main.com.promtest.data.Order;

public class AdapterOrdersList extends BaseAdapter {
    private List<Order> orders;

    public AdapterOrdersList(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Order getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_lv_orders, parent, false);
            holder = new ViewHolder();
            holder.tvFirstInfo = (TextView) convertView.findViewById(R.id.item_lv_orders_first_info);
            holder.tvSecondInfo = (TextView) convertView.findViewById(R.id.item_lv_orders_second_info);
            holder.tvTime = (TextView) convertView.findViewById(R.id.item_lv_orders_time);
            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        String name = getItem(position).getName();
        String id = "№" + getItem(position).getId();
        String time = getItem(position).getDate();
        String price = (long) getItem(position).getPriceUAH() + " " + Utils.getString(R.string.UAH);
        String state = getItem(position).getState();
        String productName = getItem(position).getItems().getAllItemsString();
        if (!state.equals(States.NEW.name))
            Utils.appendInfo(holder.tvFirstInfo, " - ", id, name, state);
        else
            Utils.appendInfo(holder.tvFirstInfo, " - ", id, name);
        Utils.appendInfo(holder.tvSecondInfo, " - ", price, productName);
        holder.tvTime.setText(time);
        Utils.setEnabledDisabledView(getItem(position).getState().equals(States.NEW.name), holder.tvFirstInfo, holder.tvSecondInfo, holder.tvTime);
        convertView.setTag(R.integer.adapter_orders_list_tag, getItem(position).getId());
        return convertView;
    }

    private class ViewHolder {
        public TextView tvFirstInfo, tvSecondInfo, tvTime;
    }


}
