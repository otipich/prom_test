package main.com.promtest.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import main.com.promtest.R;
import main.com.promtest.Utils;
import main.com.promtest.data.Item;

public class AdapterOrderDetails extends BaseAdapter {
    private List<Item> itemList;

    public AdapterOrderDetails(List<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Item getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_lv_items_list, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.item_lv_items_product_name);
            holder.tvQuantityPrice = (TextView) convertView.findViewById(R.id.item_lv_items_price_quantity);
            holder.tvTotalPrice = (TextView) convertView.findViewById(R.id.item_lv_items_total_price);
            holder.ivLogo = (ImageView) convertView.findViewById(R.id.item_lv_items_list_logo);
            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        String productName = getItem(position).getName();
        double price = getItem(position).getPrice();
        double quantity = getItem(position).getQuantity();
        String totalPrice = (long) (price * quantity) + " " + Utils.getString(R.string.UAH);
        String url = getItem(position).getImage();
        if (url != null) {
            Picasso.with(context).load(url).into(holder.ivLogo);
        }
        Utils.appendInfo(holder.tvQuantityPrice, " | ", String.valueOf((long) price) + " " + Utils.getString(R.string.UAH), String.valueOf((long) quantity) + " " + Utils.getString(R.string.count));
        holder.tvName.setText(productName);
        holder.tvTotalPrice.setText(totalPrice);
        return convertView;
    }

    private class ViewHolder {
        public TextView tvName, tvQuantityPrice, tvTotalPrice;
        public ImageView ivLogo;
    }
}
