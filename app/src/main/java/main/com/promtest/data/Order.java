package main.com.promtest.data;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Root(strict = false)
public class Order implements Serializable {
    @Attribute(name = "id")
    private int id;

    @Attribute(name = "state")
    private String state;

    @Element(name = "name", required = false)
    private String name;

    @Element(name = "phone", required = false)
    private String phone;

    @Element(name = "email", required = false)
    private String email;

    @Element(name = "date", required = false)
    private String date;

    @Element(name = "address", required = false)
    private String address;

    @Element(name = "deliveryType", required = false)
    private String deliveryType;

    @Element(name = "priceUAH", required = false)
    private double priceUAH;

    @Element(name = "company", required = false)
    private String company;

    @Element(name = "items", required = false)
    private Items items;

    private List<String> skuList = new ArrayList<>();

    private List<String> productList = new ArrayList<>();

    Order() {}

    public String getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public Items getItems() {
        return items;
    }

    public double getPriceUAH() {
        return priceUAH;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public String getAddress() {
        return address;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getCompany() {
        return company;
    }


    public List<String> getSkuList() {
        return skuList;
    }

    public List<String> getProductList() {
        return productList;
    }

    public void setProductList(List<String> productList) {
        this.productList = productList;
    }

    public void setSkuList(List<String> skuList) {
        this.skuList = skuList;
    }
}
