package main.com.promtest.data;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

import java.io.Serializable;

public class Item implements Serializable {
    @Attribute(name = "id", required = false)
    private long id;

    @Element(name = "name", required = false)
    private String name;

    @Element(name = "external_id", required = false)
    private long external_id;

    @Element(name = "quantity", required = false)
    private double quantity;

    @Element(name = "currency", required = false)
    private String currency;

    @Element(name = "image", required = false)
    private String image;

    @Element(name = "url", required = false)
    private String url;

    @Element(name = "price", required = false)
    private double price;

    @Element(name = "sku", required = false)
    private String sku;

    private int orderId;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getQuantity() {
        return quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }

    public double getPrice() {
        return price;
    }

    public String getSku() {
        return sku;
    }



    Item(){}
}