package main.com.promtest.data;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;


@Root(name = "orders", strict = false)
public class Orders extends CommonData{
    @Attribute(name = "date")
    private String date;

    @ElementList(name = "order", required = false, inline = true)
    private List<Order> orders;

    Orders() {}

    public String getDate() {
        return date;
    }

    public List<Order> getOrdersList() {
        return orders;
    }
}
