package main.com.promtest.data;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

@Root(strict = false)
public class Items implements Serializable{
    @ElementList(name = "item", required = false, inline = true)
    private List<Item> item;

    Items() {
    }

    public List<Item> getItemsList() {
        return item;
    }

    public String getAllItemsString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < getItemsList().size(); i++) {
            result.append(item.get(i).getName());
            if (i < getItemsList().size() - 1) {
                result.append(",");
            }
        }
        return result.toString();
    }

    public long getAllPrice(){
        long result = 0;
        for (Item item: getItemsList()){
            result += Math.round(item.getPrice()) * (long)item.getQuantity();
        }
        return result;
    }
}
