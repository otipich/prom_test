package main.com.promtest;


public enum States {
    NEW("new"),
    ACCEPTED("accepted"),
    DECLINED("declined"),
    DRAFT("draft"),
    CLOSED("closed");

    public String name;

    States(String name) {
        this.name = name;
    }
}
