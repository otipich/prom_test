package main.com.promtest.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import main.com.promtest.R;
import main.com.promtest.Utils;
import main.com.promtest.adapters.AdapterSearch;
import main.com.promtest.core.BaseFragment;
import main.com.promtest.core.Model;
import main.com.promtest.data.Item;
import main.com.promtest.data.Order;

public class FrgSearch extends BaseFragment implements TextWatcher, AdapterView.OnItemClickListener, View.OnClickListener {
    private List<Order> orders;
    private ListView lvSearch;
    private boolean isKeyboardShowed = false;

    public static FrgSearch newInstance() {
        return new FrgSearch();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        EditText etSearchField = (EditText) view.findViewById(R.id.search_bar_view_search_field);
        etSearchField.addTextChangedListener(this);
        etSearchField.setVisibility(View.VISIBLE);
        ImageView ivClose = (ImageView) view.findViewById(R.id.search_bar_view_search_clear);
        ivClose.setVisibility(View.VISIBLE);
        ivClose.setOnClickListener(this);
        lvSearch = (ListView) view.findViewById(R.id.frg_search_lv);
        lvSearch.setOnItemClickListener(this);
        orders = Model.getInstance().getOrders().getOrdersList();
        if (!isKeyboardShowed) {
            Utils.showKeyboard(etSearchField);
            isKeyboardShowed = true;
        }
        setFragmentTitle(getString(R.string.search));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String toCompare = "";
        List<Item> itemsFind = new ArrayList<>();
        for (Order order : orders) {
            int id = order.getId();
            String name = order.getName().toLowerCase();
            String phone = order.getPhone().toLowerCase();

            if (!s.toString().isEmpty())
                toCompare = s.toString().toLowerCase();
            if (toCompare.length() > 2) {
                if (name.contains(toCompare)) {
                    itemsFind.addAll(order.getItems().getItemsList());
                } else if (phone.contains(toCompare)) {
                    itemsFind.addAll(order.getItems().getItemsList());
                } else if (String.valueOf(id).toLowerCase().contains(toCompare)) {
                    itemsFind.addAll(order.getItems().getItemsList());
                }
                for (Item item : order.getItems().getItemsList()) {
                    if (item.getSku() != null && item.getSku().toLowerCase().contains(toCompare))
                        itemsFind.add(item);
                    else if (item.getName() != null && item.getName().toLowerCase().contains(toCompare))
                        itemsFind.add(item);
                }
            }
        }
        AdapterSearch adapter = new AdapterSearch(itemsFind);
        lvSearch.setAdapter(adapter);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int orderId = (int) view.getTag(R.integer.adapter_search_list_tag);
        Order order = Model.getInstance().getOrderByItemId(orderId);
        getActMain().showFragment(FrgOrderDetails.newInstance(order), true);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.search_bar_view_search_clear: {
                finish();
                break;
            }
        }
    }
}
