package main.com.promtest.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import main.com.promtest.OnSwipeTouchListener;
import main.com.promtest.R;
import main.com.promtest.Utils;
import main.com.promtest.adapters.AdapterOrderDetails;
import main.com.promtest.core.App;
import main.com.promtest.core.BaseFragment;
import main.com.promtest.core.Model;
import main.com.promtest.data.Order;

public class FrgOrderDetails extends BaseFragment {
    private static final String ORDER_BUNDLE_KEY = "order_bundle_key";
    private TextView tvOrderDetailsNumber;
    private TextView tvOrderDetailsName;
    private TextView tvOrderDetailsDate;
    private TextView tvOrderDetailsPhone;
    private TextView tvOrderDetailsEmail;
    private TextView tvOrderDetailsAddress;
    private TextView tvOrderDetailsCompany;
    private ListView lvOrderDetailsLv;
    private Order order;

    public static FrgOrderDetails newInstance(Order order) {
        Bundle args = new Bundle();
        args.putSerializable(ORDER_BUNDLE_KEY, order);
        FrgOrderDetails fragment = new FrgOrderDetails();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frg_order_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        tvOrderDetailsNumber = (TextView) view.findViewById(R.id.frg_order_details_number);
        tvOrderDetailsName = (TextView) view.findViewById(R.id.frg_order_details_name);
        tvOrderDetailsDate = (TextView) view.findViewById(R.id.frg_order_details_date);
        tvOrderDetailsPhone = (TextView) view.findViewById(R.id.frg_order_details_phone);
        tvOrderDetailsEmail = (TextView) view.findViewById(R.id.frg_order_details_email);
        tvOrderDetailsAddress = (TextView) view.findViewById(R.id.frg_order_details_address);
        tvOrderDetailsCompany = (TextView) view.findViewById(R.id.frg_order_details_company);
        lvOrderDetailsLv = (ListView) view.findViewById(R.id.frg_order_details_lv);
        LinearLayout llContainer = (LinearLayout) view.findViewById(R.id.frg_order_details_container);
        llContainer.setOnTouchListener(listener);
        order = (Order) getArguments().getSerializable(ORDER_BUNDLE_KEY);
        if (order != null) {
            fillInfo();
        }
        setFragmentTitle(getString(R.string.order_info));
    }

    private void fillInfo() {
        tvOrderDetailsNumber.setText(Utils.format(order.getId(), R.string.order_id));
        tvOrderDetailsName.setText(Utils.format(order.getName(), R.string.client_name));
        tvOrderDetailsDate.setText(Utils.format(order.getDate(), R.string.date));
        tvOrderDetailsPhone.setText(Utils.format(order.getPhone(), R.string.phone_number));
        tvOrderDetailsEmail.setText(Utils.format(order.getEmail(), R.string.email));
        tvOrderDetailsAddress.setText(Utils.format(order.getAddress(), R.string.address));
        if (order.getCompany() == null)
            tvOrderDetailsCompany.setVisibility(View.GONE);
        else
            tvOrderDetailsCompany.setText(Utils.format(order.getCompany(), R.string.company));
        AdapterOrderDetails adapter = new AdapterOrderDetails(order.getItems().getItemsList());
        lvOrderDetailsLv.setAdapter(adapter);
        lvOrderDetailsLv.addFooterView(createFooter(order.getItems().getAllPrice() + " " + Utils.getString(R.string.UAH)));
    }


    private OnSwipeTouchListener listener = new OnSwipeTouchListener(App.getContext()) {
        public void onSwipeRight() {
            if (Model.getInstance().getPreviousOrder(order) != null) {
                finish();
                getActMain().showFragment(FrgOrderDetails.newInstance(Model.getInstance().getPreviousOrder(order)), true);
            }
        }

        public void onSwipeLeft() {
            if (Model.getInstance().getNextOrder(order) != null) {
                finish();
                getActMain().showFragment(FrgOrderDetails.newInstance(Model.getInstance().getNextOrder(order)), true);
            }
        }
    };

    private View createFooter(String text) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.lv_items_footer, null);
        ((TextView) v.findViewById(R.id.lv_items_footer_text)).setText(text);
        return v;
    }
}
