package main.com.promtest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import main.com.promtest.R;
import main.com.promtest.Utils;
import main.com.promtest.adapters.AdapterOrdersList;
import main.com.promtest.core.App;
import main.com.promtest.core.BaseFragment;
import main.com.promtest.core.Model;
import main.com.promtest.data.CommonData;
import main.com.promtest.data.Order;
import main.com.promtest.data.Orders;
import main.com.promtest.server.Server;
import main.com.promtest.server.request.ICallback;

public class FrgMain extends BaseFragment implements ICallback, AdapterView.OnItemClickListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private ListView lvOrders;
    private Orders orders;
    private SwipeRefreshLayout refreshLayout;


    public static FrgMain newInstance() {
        return new FrgMain();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fgr_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        lvOrders = (ListView) view.findViewById(R.id.frg_main_lv_with_orders);
        lvOrders.setOnItemClickListener(this);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.frg_main_swipe_container);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Utils.getColor(R.color.colorPrimary),Utils.getColor(R.color.colorPrimaryDark), Utils.getColor(R.color.colorAccent));
        LinearLayout llSearchContainer = (LinearLayout) view.findViewById(R.id.search_container);
        llSearchContainer.setOnClickListener(this);
        setFragmentTitle(getString(R.string.orders));
        if (!Utils.hasConnection())
            Toast.makeText(App.getContext(), Utils.getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
        else if (orders == null) {
            Server.getAllOrders(this);
        } else
            fillListView(orders);
    }


    @Override
    public void onResponseResult(CommonData commonData) {
        if (commonData instanceof Orders) {
            Model.getInstance().setOrders((Orders) commonData);
            orders = (Orders) commonData;
            fillListView(orders);
            refreshLayout.setRefreshing(false);
        }
    }

    private void fillListView(Orders orders) {
        AdapterOrdersList adapter = new AdapterOrdersList(orders.getOrdersList());
        lvOrders.setAdapter(adapter);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int orderId = (int) view.getTag(R.integer.adapter_orders_list_tag);
        Order order = Model.getInstance().getOrderById(orderId);
        getActMain().showFragment(FrgOrderDetails.newInstance(order), true);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.search_container: {
                if (!Utils.hasConnection())
                    Toast.makeText(App.getContext(), Utils.getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                else
                    getActMain().showFragment(FrgSearch.newInstance(), true);
                break;
            }
        }
    }

    @Override
    public void onRefresh() {
        if (!Utils.hasConnection()) {
            Toast.makeText(App.getContext(), Utils.getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
            refreshLayout.setRefreshing(false);
        } else {
            refreshLayout.setRefreshing(true);
            Server.getAllOrders(this);
        }
    }
}
